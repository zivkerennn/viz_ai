examin_dicoms
================
This code downloads a zipped tar archive from a given link, saves it locally to a given folder.
In the same folder, all dicoms are extracted from the archive and arranged in a directory structure according to this hierarchy:
patient--> study --> series.
The following information is extracted from the data:
1. A list of patients, each consisting of the patient's ID, Sex and age:
'1.3.12.2.1107.5.1.4.0.30000016082001341060900000829, F, 060Y'
2. Average scan time
3. Number of hospitals from which the dicoms have arrived.

Input:
------
run the main.py with 2 arguments:
python main.py <LINK> <FOLDER_NAME>
LINK - URL of a tar file containing multiple DICOM files of several patients
FOLDER_NAME - local directory to which the tar archive will be downloaded, and in which the dicoms will be organized in a directory structure.

example:
python main.py 'https://s3.amazonaws.com/viz_data/DM_TH.tgz' 'Q:/Private/learnPython/vizInput'

Output:
--------
1. a results object with this format:
{'patients_list': [...], 'avg_scan_time': 33, 'number_of_hospitals': 3}
each patient on the 'patients_list' is a string consisting of:
patientID, sex, age

2. A directory structure in the local directory given by the user.

---------------

# Assumptions:
1. scan time is the difference between the last image of the series and the first image of the series
2. the time stamp used for a dicom's recording is dicom.ContentTime
3. the dicoms arrive in order of acquisition, therefore, arrays of times in the function handle_dicoms.calc_scan_times are not sorted