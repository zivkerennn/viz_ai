from setuptools import setup

setup(name='examin_dicoms',
      version='0.1',
      description='examinDicoms',
      author='Keren Ziv',
      author_email='zivkerennn@gmail.com',
      license='mine',
      packages=['examin_dicoms'],
      install_requires = [
            'numpy',
            'pydicom'
      ],
      zip_safe=False)