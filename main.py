import tarfile
import sys
import wget
import os
from examine_dicoms.handle_dicoms import extract_dicom_data

print ('Arguments List:', str(sys.argv))
print ('Start download the file')

url = sys.argv[1]
downloadDir = sys.argv[2]

wget.download(url, downloadDir)
archiveFolder = os.listdir(downloadDir)
tarArchive = tarfile.open(os.path.join(downloadDir,archiveFolder[0]), "r:gz")
dicomsDir = os.path.join(downloadDir, 'dicomsDir')
tarArchive.extractall(dicomsDir)

resultsObj = extract_dicom_data(dicomsDir)
print(resultsObj)