import os, shutil
import pydicom
from datetime import datetime
import statistics

def calc_scan_times(acqTimesMap):
    # Assumptions:
    # 1. scan time is the difference between the last image of the series and the first image of the series
    # 2. the time stamp used is ContentTime
    # 3. the dicoms arrive in order of acquisition, therefore, arrays of times in acqTimesMap are not sorted in this function
    allScanTimes = []
    secondsInDay = 86400  #24 * 60 * 60
    for timeSeries in acqTimesMap:
        scanTimeDelta = acqTimesMap[timeSeries][(len(acqTimesMap[timeSeries])-1)] - acqTimesMap[timeSeries][0]
        allScanTimes.append((scanTimeDelta.days * secondsInDay + scanTimeDelta.seconds))

    return statistics.mean(allScanTimes)

def extract_dicom_data(tmpDir):
    # this function extracts dicom data and arranges the directory structures
    dicomsDir = os.listdir(tmpDir)
    hospitalsList = set()
    patientsList = set()
    acqTimesMap = {}
    for dicomFile in dicomsDir:
        dicomFilePath = os.path.join(tmpDir, dicomFile)
        dcm = pydicom.dcmread(dicomFilePath)

        """ Extract Dicom Info """
        patientId = dcm.PatientName.family_name
        studyId = dcm.StudyInstanceUID
        seriesId = dcm.SeriesInstanceUID
        patientInfo = patientId + ', ' + dcm.PatientSex + ', ' + dcm.PatientAge
        patientsList.add(patientInfo)
        hospitalsList.add(dcm.InstitutionName)
        ContentTime = datetime.strptime(dcm.ContentTime.split('.')[0], '%H%M%S')
        if not(dcm.SeriesInstanceUID in acqTimesMap):
            acqTimesMap[dcm.SeriesInstanceUID] = []
        acqTimesMap[dcm.SeriesInstanceUID].append(ContentTime)

        """ Organize Folders """
        patientDir = os.path.join(tmpDir, patientId)
        if not(os.path.isdir(patientDir)):
            os.mkdir(patientDir)

        studyDir = os.path.join(patientDir, studyId)
        if not(os.path.isdir(studyDir)):
            os.mkdir(studyDir)

        seriesDir = os.path.join(studyDir, seriesId)
        if not (os.path.isdir(seriesDir)):
            os.mkdir(seriesDir)

        shutil.move(dicomFilePath, seriesDir)

    avgScanTime = calc_scan_times(acqTimesMap)

    return {'patients_list': patientsList, 'avg_scan_time':avgScanTime, 'number_of_hospitals': len(hospitalsList)}



